# MInecraft JE docker-compose 
## File descriptions
### server.properties

In `.app/server.properties` file you could setup parameters of the MC server
The full list of propertys description available via the [link](https://minecraft.fandom.com/wiki/Server.properties)

### whitelist.json
In `.app/whitelist.json` file you could setup whitelist of the players
```json
[
    {
      "uuid": "976833c1-dd1b-4bd2-b561-5fe482h81001",
      "name": "player1"
    },
    {
      "uuid": "072023a3-b561-2bs2-b561-4fe352dh1601",
      "name": "player2"
    }
  ]
```
To take `UUID` of the user just check logs of your container `docker logs  mc-je-su -f` when user trying to log in to the serve and copy it to the  JSON. (Don't forget to restart container, to apply changes)


### 

The container starts with UID=1000 & GID=1000
to check groups

cat /etc/passwd

sudo chown opc:opc server.properties